# gbtracker

The project is all about reliable tracking of  a golfball, especially in low velocity shots like chips and putts. In the first step the goal is to write code which implements this tracking using an iPhone camera.
In the second part of this project, the goal is to combine the ball tracking part with apple's ARKit framework to create some kind of training tool to use around the practice grounds of a golfcourse.